/**
 *
 */
(function () {
    window.komponentor = {
        components: {}
    };

    /**
     *
     * @param $el
     * @param parent
     * @returns {null|{parent: *, $el: *}}
     * @constructor
     */
    function Komponent($el,parent) {
        if($el.hasOwnProperty("$el"))
            return  $el;

        if($el.hasOwnProperty("length") && $el.data("komponent"))
            return $el.data("komponent");

        console.log($el,parent);

        let data = {
            $el: $el,
            parent: parent,
            komponents: {}
        };

        if(parent) {
            if(parent.hasOwnProperty("length")) {
                // is jquery el
                if(parent.data("komponent"))
                    parent = parent.data("komponent");
                else
                    parent = Komponent(parent);
            }

            if(!parent.hasOwnProperty("$el")) {
                return null;
            }

            data.parent = parent;
        }

        let dataAttrs = $el.data();
        delete dataAttrs["komponent"];
        Object.assign(data,);

        if(!$el.attr("id") && $el.data("url")) {
            $el.attr("id",$el.data("url").hashCode());
        }



        if(data.parent)
            data.parent.komponents[$el.attr("id")] = data;

        $el.data("komponent",data);

        return $el.data("komponent");
    }

    String.prototype.hashCode = function()
    {
        var hash = 0, i, chr;
        if (this.length === 0) return hash;
        for (i = 0; i < this.length; i++) {
            chr   = this.charCodeAt(i);
            hash  = ((hash << 5) - hash) + chr;
            hash |= 0; // Convert to 32bit integer
        }
        return Math.abs(hash);
    };

    /**
     *
     * @param cId
     * @param data
     * @param el
     * @returns {*}
     */
    function loadComponent(options, data)
    {
        console.log(options);
        return new Promise( (resolve,reject)=>{
            let cId = options.url.hashCode();
            // no default container, create one on document root
            if(!options.$el) {
                if(!options.hasOwnProperty("parent"))
                    reject("No valid parent");

                options.$el = $("<div>").appendTo(options.parent.$el).attr("id", cId).css("display", "none");
            }

            let k = Komponent(options.$el.html(data),options.parent);

            let initFunc = typeof init==="function"?init:()=>{};
            init = null;

            if(k.hasOwnProperty("loadrecursive") && k.loadrecursive === "inhibit")
                return resolve(k);

            loadComponents(k.$el)
                .then(()=>{
                    initFunc(k,data);
                    resolve(k);
                })
                .catch((a)=>reject(a));
        })
    }

    /**
     *

     * @param el
     * @returns {Promise<unknown>}
     */
    komponentor.getComponent = function(opts)
    {
        console.log(opts);
        return new Promise((resolve,reject)=>{
            let options = {
                $el:null,
                url:null,
                parent: $("body")
            };
            Object.assign(options,opts);

            if(options.$el && options.$el.data("komponent")) {
                resolve(options.$el.data("komponent"));
                return;
            }

            if(!options.url)
                options.url = options.$el?options.$el.data("url"):null;

            if (!options.url && !options.$el) {
                reject(JSON.stringify(options));
                return;
            }

            if(options.$el && !options.url) {
                return Komponent(options.$el,options.parent);
            }

            options.parent = Komponent(options.parent);

            let cid = options.url.hashCode();
            if(options.parent && options.parent.komponents.hasOwnProperty(cid)) {
                resolve(options.parent.komponents[cid]);
                return;
            }

            // load component
            $.get(options.url)
                .done(function (data) {
                    loadComponent(options,data)
                        .then((comp)=>resolve(comp))
                        .catch((cause)=>reject(cause));
                })
                .fail(function (xhr) {
                    reject(xhr);
                });
        });
    }

    function basicComponent() {

    }
    /**
     *
     * @param parent
     * @returns {Promise<unknown>}
     */
    function loadComponents(parent)
    {
        let parentKomponent = $(parent).data("komponent");

        if(!parentKomponent)
            parentKomponent = Komponent($(parent));

        return new Promise(
            function(resolve,reject)
            {
                let deep;
                let counter = 0;
                $(parent).find("[is=component]").each(function () {
                    counter++;
                    komponentor.getComponent({$el:$(this),parent:$(parent)})
                        .then((komponent) => {
                            parentKomponent.komponents[komponent.$el.attr("id")] = komponent;
                        })
                        .finally(() => counter--);
                });

                function wait(to) {
                    deep++;
                    if(deep>10)
                        resolve();
                    setTimeout(()=> {
                        if(counter>0)
                            wait(to);
                        else
                            resolve();
                    },to);
                }
                wait(200);
            });
    }


    $(document).ready(function () {
        //if(typeof komponentorAutoload!=="undefined" && komponentorAutoload)
        loadComponents("body");
    });


    komponentor.sendIntent = function(options)
    {
        komponentor.getComponent(options)
            .then(  function (c) {
                console.log(c);
                if(!c)
                    return;
                if(c.hasOwnProperty("exec"))
                    c.exec(options.data,options.callback);
            })
            .catch(function (v) {
                console.log("could not load",v);
            });
    }
})();
