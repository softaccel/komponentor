/**
 *
 */
(function () {

    String.prototype.hashCode = function()
    {
        var hash = 0, i, chr;
        if (this.length === 0) return hash;
        for (i = 0; i < this.length; i++) {
            chr   = this.charCodeAt(i);
            hash  = ((hash << 5) - hash) + chr;
            hash |= 0; // Convert to 32bit integer
        }
        return Math.abs(hash);
    };

    window.componentor = {
        components: {},
        sendIntent: function (uri, data, callback) {
            getComponent(uri)
                .then(function (c) {
                    if (!c)
                        return;
                    if (c.hasOwnProperty("exec"))
                        c.exec(data, callback);
                })
                .catch(function (v) {
                    console.log("could not load", v);
                });
        }
    };

    /**
     *
     * @param cId
     * @param data
     * @param container
     * @returns {*}
     */
    function loadComponent(cId, data,container)
    {
        return new Promise( (resolve,reject)=>{
            if(!container)
                container = $("<div>").attr("id",cId).css("display","none").appendTo("body");
            console.log(container);
            container = $(container);
            let $component = container.html(data);

            let initFunc = typeof init==="function"?init:()=>{};


            loadComponents($component)
                .then(()=>{
                    componentor.components[cId] = initFunc($component,data);
                    resolve(componentor.components[cId]);
                })
                .catch((a)=>reject(a));
        })
    }

    /**
     *
     * @param uri
     * @param paras
     * @param container
     * @returns {Promise<unknown>}
     */
    function getComponent(uri,paras,container)
    {
        let cId = uri.hashCode();

        return  new Promise(function (resolve,reject) {
            if(componentor.components.hasOwnProperty(cId)) {
                resolve(componentor.components[cId]);
            }
            else {
                // load component
                $.get(uri)
                    .done(function (data) {
                        loadComponent(cId,data,container)
                            .then((comp)=>resolve(comp))
                            .catch((cause)=>reject(cause));
                    })
                    .fail(function (xhr) {
                        reject(xhr);
                    });
            }
        });
    }

    /**
     *
     * @param parent
     * @returns {Promise<unknown>}
     */
    function loadComponents(parent)
    {
        return new Promise(
            function(resolve,reject)
            {
                let deep;
                let counter = 0;
                $(parent).find("[is=component]").each(function () {
                    let uri = $(this).data("uri");
                    let data = $(this).attr("data");
                    if (data)
                        data = JSON.parse(data);
                    if (!uri)
                        return;

                    counter++;
                    getComponent(uri, data, this).then((c) => console.log(c, this)).finally(() => counter--);
                });

                function wait(to) {
                    deep++;
                    if(deep>10)
                        resolve();
                    setTimeout(()=> {
                        if(counter>0)
                            wait(to);
                        else
                            resolve();
                    },to);
                }
                wait(200);
            });

    }


    $(document).ready(function () {
        loadComponents("body");
    });
})();
